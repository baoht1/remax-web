import { useRecoilState } from "recoil";

import { cartState, customerInfoState } from "./Atoms";

export const useAddProduct = () => {
  const [cart, setCart] = useRecoilState(cartState);

  return (product, amount) => {
    const index = cart.findIndex((item) => item.id === product.id);

    if (index === -1) {
      return setCart([...cart, { ...product, qty: amount }]);
    }

    const newCart = cart.map((item, i) => {
      if (i === index) {
        return {
          ...item,
          qty: item.qty + amount,
        };
      }

      return item;
    });
    setCart(newCart);
  };
};
export const useMinusProduct = () => {
  const [cart, setCart] = useRecoilState(cartState);

  return (product, amount) => {
    const index = cart.findIndex((item) => item.id === product.id);

    if (index === -1) {
      return setCart([...cart, { ...product, qty: amount }]);
    }

    const newCart = cart.map((item, i) => {
      if (i === index) {
        return {
          ...item,
          qty: item.qty - amount,
        };
      }

      return item;
    });

    setCart(newCart);
  };
};

export const useRemoveProduct = () => {
  const [cart, setCart] = useRecoilState(cartState);

  return (product) => {
    // const index = cart.findIndex((item) => item.id === product.id);

    // if (index === -1) {
    //   alert("Product not found in cart!");
    //   return;
    // }

    const newCart = [];
    cart.map(e=>{
      if(e.id !== product.id){
        newCart.push(e)
      }
    })
    setCart(newCart);
  };
};
export const removeProductAll = () => {
  const [cart, setCart] = useRecoilState(cartState);
  return () => {
   setCart([])
  };
};
export const updateCustomerInfo = () => {
  const [customerInfo, setcustomerInfo] = useRecoilState(customerInfoState);

  return (customerInfo) => {
    setcustomerInfo(customerInfo);
  };
};
