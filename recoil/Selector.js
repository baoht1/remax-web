import { selector } from "recoil";

import { cartState, customerInfoState } from "./Atoms";

export const cartStatus = selector({
  key: "cartStatus",
  get: ({ get }) => {
    const cart = get(cartState);
    const totalItems = cart.length;
    const item = cart;

    return {
      itemLength: totalItems,
      item,
    };
  },
});
export const customerInfomation = selector({
  key: "customerInfoState",
  get: ({ get }) => {
    const info = get(customerInfoState);
    return info;
  },
});
