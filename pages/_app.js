import { createGlobalStyle, ThemeProvider } from "styled-components";
import "antd/dist/antd.css";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Head from "next/head";
import { RecoilRoot } from "recoil";
const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
    background: #ececec;
    ${
      "" /* min-width : ${(props) => (props.width ? props.width+"px" : "100%")}; */
    }
    ${"" /* overflow :auto */}
  }
`;

const theme = {
  colors: {
    primary: "#0070f3",
  },
};

export default function App({ Component, pageProps }) {
  // const width = window ? window.outerWidth : 1080;
  return (
    <>
      <Head>
        <title>Shop phụ kiện điện thoại</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <GlobalStyle />
      <RecoilRoot>
        <ThemeProvider theme={theme}>
          <Component {...pageProps} />
        </ThemeProvider>
      </RecoilRoot>
    </>
  );
}
