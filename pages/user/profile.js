import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Button,
  Checkbox,
  DatePicker,
  Radio,
  Select,
  Avatar,
  Upload,
} from "antd";
import Head from "./../../components/Header";
import SliderUser from "./../../components/SliderUser";
import styled from "styled-components";
import Footer from "./../../components/Footer"
import {
  UploadOutlined,
  EditOutlined,
  UserOutlined,
  MailOutlined,
  AppstoreOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import { useRouter } from "next/router";
const Body = styled.div`
  h2 {
    margin-bottom: 0.3em;
  }
  .divider {
    border-bottom: solid 1px #efefef;
    margin: 10px 0;
  }
  .left-content {
    width: 60%;
    float: left;
  }
  .right-content {
    width: 30%;
    float: right;
    text-align: center;
    border-left : solid 1px #efefef;
  }
  .upload{
      display : block;
      margin-top : 10px;
  }
`;
const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};
const { Option } = Select;
const Main = (props) => {
  let router = useRouter();
  let path = router.pathname;
  return (
    <>
      <Head />
      <Body>
        <Row justify="center">
          <Col span={20}>
            <Row justify="center">
              <SliderUser path={path}/>
              <Col span={20}>
                <Card className="card-userInfo">
                  <h2>Hồ Sơ Của Tôi</h2>
                  <span>Quản lý thông tin hồ sơ để bảo mật tài khoản</span>
                  <div className="divider"></div>
                  <div className="left-content">
                    <Form
                      {...layout}
                      name="basic"
                      //   initialValues={{ remember: true }}
                      //   onFinish={onFinish}
                      //   onFinishFailed={onFinishFailed}
                    >
                      <Form.Item
                        label="Tên đăng nhập"
                        name="username"
                        initialValues="thaibao12"
                        rules={[
                          {
                            required: true,
                            message: "Please input your username!",
                          },
                        ]}
                      >
                        <Input disabled />
                      </Form.Item>
                      <Form.Item
                        label="Tên"
                        name="username"
                        initialValues="thaibao12"
                        rules={[
                          {
                            required: true,
                            message: "Please input your username!",
                          },
                        ]}
                      >
                        <Input />
                      </Form.Item>
                      <Form.Item
                        label="Ngày sinh"
                        name="birthday"
                        rules={[
                          {
                            required: true,
                            message: "Please input your username!",
                          },
                        ]}
                      >
                        <DatePicker
                          style={{ width: "100%" }}
                          placeholder="Vui lòng chọn ngày sinh"
                        />
                      </Form.Item>
                      <Form.Item
                        label="Giới tính"
                        name="gender"
                        initialValues="thaibao12"
                        rules={[
                          {
                            required: true,
                            message: "Please input your username!",
                          },
                        ]}
                      >
                        <Radio.Group>
                          <Radio value="a">Nam</Radio>
                          <Radio value="b">Nữ</Radio>
                          <Radio value="c">Khác</Radio>
                        </Radio.Group>
                      </Form.Item>

                      <Form.Item
                        label="Địa chỉ"
                        name="address"
                        rules={[
                          {
                            required: true,
                            message: "Please input your password!",
                          },
                        ]}
                      >
                        <Input />
                      </Form.Item>

                      <Form.Item
                        label="Tỉnh"
                        name="province"
                        rules={[
                          {
                            required: true,
                            message: "Please input your password!",
                          },
                        ]}
                      >
                        <Select placeholder="Please select a country">
                          <Option value="china">China</Option>
                          <Option value="usa">U.S.A</Option>
                        </Select>
                      </Form.Item>
                      <Form.Item
                        label="Huyện"
                        name="district"
                        rules={[
                          {
                            required: true,
                            message: "Please input your password!",
                          },
                        ]}
                      >
                        <Select placeholder="Please select a country">
                          <Option value="china">China</Option>
                          <Option value="usa">U.S.A</Option>
                        </Select>
                      </Form.Item>
                      <Form.Item
                        label="Xã"
                        name="ward"
                        rules={[
                          {
                            required: true,
                            message: "Please input your password!",
                          },
                        ]}
                      >
                        <Select placeholder="Please select a country">
                          <Option value="china">China</Option>
                          <Option value="usa">U.S.A</Option>
                        </Select>
                      </Form.Item>
                      <Form.Item
                        label="Password"
                        name="password"
                        rules={[
                          {
                            required: true,
                            message: "Please input your password!",
                          },
                        ]}
                      >
                        <Input.Password />
                      </Form.Item>

                      <Form.Item {...tailLayout}>
                        <Button type="primary" htmlType="submit">
                          Lưu
                        </Button>
                      </Form.Item>
                    </Form>
                  </div>
                  <div className="right-content">
                    <Avatar size={100} icon={<UserOutlined />} className="avatar"/>
                    <Upload className="upload">
                      <Button icon={<UploadOutlined />}>Chọn ảnh</Button>
                    </Upload>
                    ,
                  </div>
                </Card>
              </Col>
            </Row>
          </Col>
        </Row>
      </Body>
      <Footer /> 
    </>
  );
};
export default Main;
