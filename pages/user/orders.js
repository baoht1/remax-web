import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Button,
  Checkbox,
  DatePicker,
  Radio,
  Select,
  Avatar,
  Tabs,
} from "antd";
import Head from "./../../components/Header";
import SliderUser from "./../../components/SliderUser";
import styled from "styled-components";
import Footer from "./../../components/Footer";
import TabContent from "./../../components/TabContent";
import {
  UploadOutlined,
  EditOutlined,
  UserOutlined,
  MailOutlined,
  AppstoreOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import { useRouter } from "next/router";
import helper from "./../../services/helper";
import {useEffect } from "react"
const Body = styled.div`
  h2 {
    margin-bottom: 0.3em;
  }
  .divider {
    border-bottom: solid 1px #efefef;
    margin: 10px 0;
  }
  .left-content {
    width: 60%;
    float: left;
  }
  .right-content {
    width: 30%;
    float: right;
    text-align: center;
    border-left: solid 1px #efefef;
  }
  .ant-tabs-nav-list {
    width: 100%;
  }
  .ant-tabs-nav {
    background-color: white;
  }
  .ant-tabs-tab {
    width: 16.6%;
    justify-content: center;
    font-size: 1.2em;
    font-weight: 600;
    color: black;
    margin: 0 !important;
  }
  .ant-tabs-tab-active .ant-tabs-tab-btn {
    color: #ee4d2d;
    border-color: #ee4d2d;
  }
  .ant-tabs-ink-bar {
    background: #ee4d2d;
  }
  .card-order {
    min-height: 500px;
  }
`;
const layout = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};
const { TabPane } = Tabs;
const Main = (props) => {
  let router = useRouter();
  let path = router.pathname;
  useEffect(() => {
    if(!helper.isLogin()){
      router.push("/login")
    }
  }, []);
  return (
    <>
      <Head />
      <Body>
        <Row justify="center">
          <Col span={20}>
            <Row justify="center">
              <SliderUser path={path} />
              <Col span={20}>
                {/* <Card className="card-order"> */}
                <Tabs defaultActiveKey="1" centered className="tabs">
                  <TabPane tab="Tất cả" key="1">
                    <TabContent key={1} keyOrder = {1}/>
                  </TabPane>
                  <TabPane tab="Chờ xác nhận" key="2">
                    <TabContent key={2}  keyOrder = {2}/>
                  </TabPane>
                  <TabPane tab="Chờ lấy hàng" key="3" >
                    <TabContent key={3} keyOrder = {3}/>
                  </TabPane>

                  <TabPane tab="Đang giao" key="4" >
                    <TabContent key={4} keyOrder = {4}/>
                  </TabPane>

                  <TabPane tab="Đã giao" key="5" >
                    <TabContent key={5} keyOrder = {5}/>
                  </TabPane>

                  <TabPane tab="Đã hủy" key="6">
                    <TabContent key={6}  keyOrder = {6}/>
                  </TabPane>
                </Tabs>
                {/* </Card> */}
              </Col>
            </Row>
          </Col>
        </Row>
      </Body>
      <Footer />
    </>
  );
};
export default Main;
