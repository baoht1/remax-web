import {
  Row,
  Col,
  Input,
  Menu,
  Badge,
  Affix,
  Breadcrumb,
  Card,
  Divider,
  Select,
  Button,
  Comment,
  List,
  Rate,
  Form,
  message,
} from "antd";
import styled from "styled-components";
import Head from "../../components/Header";
import Slider from "react-slick";
import React, { useState, useEffect } from "react";
import { CarOutlined, DollarOutlined } from "@ant-design/icons";
import ListProduct from "../../components/ListProduct";
import Footer from "../../components/Footer";
import clientData from "../../services/clientData";
import { useRouter } from "next/router";
import { useAddProduct, useRemoveProduct } from "../../recoil/hooks";
import helper from "./../../services/helper";
const { TextArea } = Input;
const Body = styled.div`
  .slick-vertical {
    .slick-track {
      height: 0px !important;
    }
  }
  .slick-image {
    padding: 0px 10px;
    img {
      max-height: 432px;
    }
  }
  .has-border {
    border-bottom: 1px solid #fbf1f1;
  }
  .white-background {
    margin-bottom: 10px;
  }
  .price-product {
    color: #d30606;
    font-weight: 700;
    font-size: 20px;
    position: relative;
    top: 10px;
  }
  .rm-price {
    padding: 0 0 14px 0;
  }
  .rm-shipping {
    padding: 5px 0;
  }
  .name-product {
    margin: 0px;
  }
  .breadcrumb-link {
    margin: 4px 0;
  }
  .slick-dots {
    bottom: 0;
  }
  .breadcrumb-active {
    color: #d30606;
  }
  .rm-payment {
    padding: 5px 0;
  }
  .vnpay-image {
    width: 100px;
  }
  .rm-category {
    padding: 5px 0;
  }
  .button-buy {
    padding: 5px 0;
    .button {
      width: 100%;
      height: 50px;
      font-weight: 600;
      font-size: 1.4em;
      background-color: #e11b1e;
      color: white;
    }
    .button:hover {
      background-color: white;
      color: red;
      border-color: red;
    }
  }
  .text-vnpay-des {
    padding: 7px;
    background: #e9f7ff;

    .text {
      width: 84%;
      float: right;
    }
  }
  .ant-form-item {
    margin-bottom: 10px !important;
  }
`;
const productInfos = [
  {
    id: 1,
    image:
      "https://manager.remaxvietnam.vn/asset/images/SanPham/phukiendienthoai/loabluetooth/RB-M56/av-loa-rb-m56-18012021.png?w=288",
    name: "Phu kien",
    title: "Phu kien dien thoai",
    price: 10000,
  },
  {
    id: 2,
    image:
      "https://manager.remaxvietnam.vn/asset/images/SanPham/phukiendienthoai/loabluetooth/RB-M56/av-loa-rb-m56-18012021.png?w=288",
    name: "Phu kien",
    title: "Phu kien dien thoai",
    price: 10000,
  },
  {
    id: 3,
    image:
      "https://manager.remaxvietnam.vn/asset/images/SanPham/phukiendienthoai/loabluetooth/RB-M56/av-loa-rb-m56-18012021.png?w=288",
    name: "Phu kien",
    title: "Phu kien dien thoai",
    price: 10000,
  },
  {
    id: 4,
    image:
      "https://manager.remaxvietnam.vn/asset/images/SanPham/phukiendienthoai/loabluetooth/RB-M56/av-loa-rb-m56-18012021.png?w=288",
    name: "Phu kien",
    title: "Phu kien dien thoai",
    price: 10000,
  },
  {
    id: 5,
    image:
      "https://manager.remaxvietnam.vn/asset/images/SanPham/phukiendienthoai/loabluetooth/RB-M56/av-loa-rb-m56-18012021.png?w=288",
    name: "Phu kien",
    title: "Phu kien dien thoai",
    price: 10000,
  },
];

const Product = (props) => {
  const [productSelected, setproductSelected] = useState({});
  const [amountChoose, setamount] = useState(1);
  const [productType, setproductType] = useState();
  const [products, setproducts] = useState([]);
  const [loading, setLoading] = useState(false);
  const [productsRelate, setproductsRelate] = useState([]);
  const [isLogin, setisLogin] = useState(false);
  const addProduct = useAddProduct();
  const hangdleChangeProductSelect = (value) => {
    console.log(value);
    let productInfo = products.find((e) => {
      if( e.id == value) return e
     
    });
    let productInit = productInfo;
        productInit.name = productType.name;
        productInit.imagePr = productType.image[0];
        productInit.pricePr = productType.price;
        console.log(productInit);
    setproductSelected(productInit);
  };
  const hangdleChangeAmount = (value) => {
    setamount(Number(value));
  };

  let router = useRouter();
  let { typeId } = router.query;
  // if(!typeId) return null;

  useEffect(() => {
    getData();
  }, [router.query]);

  let getData = async () => {
    if (typeId) {
      setLoading(true);
      let login = helper.isLogin();
      setisLogin(login);
      let data = await clientData.getProductTypeInfo(typeId);
      if (data.code === 0) {
        setproductType(data?.productTypeInfo);
        setproducts(data?.products);
        // if (data?.productTypeInfo.categoryId) {
        let productRelate = await clientData.getListProductTypes({
          skip: 0,
          limit: 10,
          categoryId: data?.productTypeInfo.categoryId,
        });
        if (productRelate.code === 0) {
          setproductsRelate(productRelate.data);
        }
        // }

        let productInit = data?.products[0] || {};
        productInit.name = data?.productTypeInfo?.name;
        productInit.imagePr = data?.productTypeInfo?.image[0];
        productInit.pricePr = data?.productTypeInfo?.price;
        setproductSelected(productInit);
        setLoading(false);
      } else {
        // message.error("Không tìm thấy thông tin sản phẩm.");
        }
    }
  };
  const [slider1, setSlider1] = useState();
  const [slider2, setSlider2] = useState();
  const ProductImage = [
    "https://manager.remaxvietnam.vn/asset/images/SanPham/phukienoto/cusactrenoto/rcc-227/slide/cu-sac-o-to-remax-rcc227-thiet-ke-am-07082020.jpg",
    "https://manager.remaxvietnam.vn/asset/images/SanPham/phukienoto/cusactrenoto/rcc-227/slide/cu-sac-o-to-remax-rcc227-dac-diem-noi-bat-07082020.jpg",
    "https://manager.remaxvietnam.vn/asset/images/SanPham/phukienoto/cusactrenoto/rcc-227/slide/cu-sac-o-to-remax-rcc227-thiet-ke-am-07082020.jpg",
    "https://manager.remaxvietnam.vn/asset/images/SanPham/phukienoto/cusactrenoto/rcc-227/slide/cu-sac-o-to-remax-rcc227-dac-diem-noi-bat-07082020.jpg",
    "https://manager.remaxvietnam.vn/asset/images/SanPham/phukienoto/cusactrenoto/rcc-227/slide/cu-sac-o-to-remax-rcc227-thiet-ke-am-07082020.jpg",
  ];
  const settingVerticalSwip = {
    dots: false,
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    vertical: true,
    verticalSwiping: true,
    arrows: false,
    focusOnSelect: true,
    //   adaptiveHeight : true
  };
  const settingSwip = {
    dots: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 3000,
    infinite: true,
    speed: 500,
  };

  const RenderSlideVertical = (props) => {
    let { productImages } = props;
    return (
      <Slider
        {...settingVerticalSwip}
        asNavFor={slider2}
        // ref={(slider) => setSlider1(slider)}
        className="slick-vertical"
      >
        {productImages.map((e) => {
          return <img src={e} key={e}></img>;
        })}
      </Slider>
    );
  };
  const RenderSliderImage = (props) => {
    let { productImages } = props;
    return (
      <Slider
        {...settingSwip}
        className="slick-image"
        asNavFor={slider1}
        // ref={(slider) => setSlider2(slider)}
      >
        {productImages.map((e) => {
          return <img src={e} key={e}></img>;
        })}
      </Slider>
    );
  };
  const { Option } = Select;
  const renderInfoPr = (productType, products) => {
    return (
      <div>
        <h2 className="has-border name-product">{productType?.name}</h2>
        <div className="rm-price has-border">
          <span className="price-product">
            {productType?.price.toLocaleString()}đ
          </span>
        </div>
        <div className="rm-shipping has-border">
          <span>
            <CarOutlined style={{ fontSize: "1.2em" }} />
            Giao nhanh 30p trong nội thành
          </span>
        </div>

        <div className="rm-category has-border">
          <Row>
            <Col span={4} style={{ paddingTop: "3px" }}>
              <span className="attribute-product">Phân loại:</span>
            </Col>
            <Col span={8}>
              <Select
                style={{ width: 200 }}
                placeholder="Phân loại"
                optionFilterProp="children"
                defaultValue={productSelected ? productSelected.id : ""}
                onChange={(val) => {
                  hangdleChangeProductSelect(val);
                }}
                // onChange={onChange}
                // onFocus={onFocus}
                // onBlur={onBlur}
                // onSearch={onSearch}
              >
                {/* <Option value="jack">Vàng</Option>
                <Option value="lucy">Đen</Option>
                <Option value="tom">Xanh</Option> */}
                {products?.map((e) => {
                  return (
                    <Option value={e.id} key={e.id}>
                      {e.attributeName} - {e.attributeValue}
                    </Option>
                  );
                })}
              </Select>
            </Col>
            <Col span={2} offset={1}>
              <Select
                style={{ width: 200 }}
                placeholder="Số lượng"
                optionFilterProp="children"
                // onChange={onChange}
                // onFocus={onFocus}
                // onBlur={onBlur}
                // onSearch={onSearch}
                defaultValue="1"
                onChange={(val) => {
                  hangdleChangeAmount(val);
                }}
              >
                <Option value="1">1</Option>
                <Option value="2">2</Option>
                <Option value="3">3</Option>
                <Option value="3">4</Option>
                <Option value="3">5</Option>
              </Select>
            </Col>
          </Row>
        </div>
        <div className="rm-payment has-border">
          <div>
            <span>
              <DollarOutlined style={{ fontSize: "1.2em" }} />
              Có thể thanh toán bằng VNPay
            </span>
            <span>
              <img
                src="https://vnpay.vn/wp-content/uploads/2020/07/Logo-VNPAYQR-update.png"
                className="vnpay-image"
              />
            </span>
          </div>
          <div className="text-vnpay-des">
            <img src="https://remaxvietnam.vn/images/icon/logo-vnp@2x.png"></img>
            <span className="text">
              Thanh toán bằng quét QRcode qua App của 15 ngân hàng hàng đầu.{" "}
              <a href="#"> Xem hướng dẫn và danh sách ngân hàng.</a>
            </span>
          </div>
        </div>
        <div className="button-buy">
          <Button
            className="button"
            onClick={() => addProduct(productSelected, amountChoose)}
          >
            Mua ngay
          </Button>
        </div>
      </div>
    );
  };
  const renderComment = () => {
    const Editor = ({ onChange, onSubmit, submitting, value }) => {
      if (!isLogin) return "Vui lòng đăng nhập để bình luận sản phẩm.";
      return (
        <>
          <Form.Item>
            <TextArea rows={3} onChange={onChange} value={value} />
          </Form.Item>
          <Form.Item>
            <Rate />
          </Form.Item>
          <Form.Item>
            <Button
              htmlType="submit"
              loading={submitting}
              onClick={onSubmit}
              type="primary"
            >
              Thêm bình luận sản phẩm
            </Button>
          </Form.Item>
        </>
      );
    };
    const data = [
      {
        author: "Han Solo",
        avatar:
          "https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png",
        content: <Editor />,
        datetime: <span>{new Date().getTime}</span>,
      },
      {
        author: "Han Solo",
        avatar:
          "https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png",
        content: (
          <div>
            <Rate disabled defaultValue={3} />
            <p>
              We supply a series of design principles, practical patterns and
              high quality design resources (Sketch and Axure), to help people
              create their product prototypes beautifully and efficiently.
            </p>
          </div>
        ),
        datetime: <span>{new Date().getTime}</span>,
      },
      {
        author: "Han Solo",
        avatar:
          "https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png",
        content: (
          <div>
            <Rate disabled defaultValue={2} />
            <p>
              We supply a series of design principles, practical patterns and
              high quality design resources (Sketch and Axure), to help people
              create their product prototypes beautifully and efficiently.
            </p>
          </div>
        ),
        datetime: <span>{new Date().getTime}</span>,
      },
    ];
    return (
      <List
        className="comment-list"
        itemLayout="horizontal"
        dataSource={data}
        renderItem={(item) => (
          <li>
            <Comment
              author={item.author}
              avatar={item.avatar}
              content={item.content}
              datetime={item.datetime}
            />
          </li>
        )}
      />
    );
  };
  function BodyComponent(props) {
    let { productTypeInfo, products } = props;
    let productImage = [];
    products?.map((e) => {
      productImage.push(e.image);
    });
    return (
      <Body>
        <Row justify="center">
          <Col span={20}>
            <Breadcrumb className="breadcrumb-link">
              <Breadcrumb.Item>Trang chủ</Breadcrumb.Item>
              <Breadcrumb.Item className="breadcrumb-active">
                Phụ kiện điện thoại
              </Breadcrumb.Item>
            </Breadcrumb>
          </Col>
          <Col span={20} className="white-background">
            <Card>
              <Row>
                <Col span={2}>
                  <RenderSlideVertical productImages={productImage} />
                </Col>
                <Col span={14}>
                  <RenderSliderImage productImages={productImage} />
                </Col>
                <Col span={8}>{renderInfoPr(productTypeInfo, products)}</Col>
              </Row>
            </Card>
          </Col>
          <Col span={20} className="white-background">
            <Card title="Giới thiệu sản phẩm">
              <p>{productTypeInfo?.description}</p>
            </Card>
          </Col>
          <Col span={20} className="white-background">
            <Card title="Bình luận và đánh giá">{renderComment()}</Card>
          </Col>
          <ListProduct title="Sản phẩm liên quan" products={productsRelate} />
        </Row>
      </Body>
    );
  }
  if (loading) return "Đang tải...";
  return (
    <>
      <Head />
      <BodyComponent productTypeInfo={productType} products={products} />
      <Footer />
    </>
  );
};

export default Product;
