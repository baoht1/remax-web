import {
  Row,
  Col,
  Input,
  Menu,
  Badge,
  Affix,
  Breadcrumb,
  Card,
  Divider,
  Select,
  Button,
  Comment,
  List,
  Rate,
  Form,
  message,
} from "antd";
import styled from "styled-components";
import Head from "./../components/Header";
import Footer from "./../components/Footer";
import { useRecoilValue } from "recoil";
import { cartStatus } from "../recoil/Selector";
import { useState } from "react";
import { PlusOutlined, MinusOutlined } from "@ant-design/icons";
import {
  useAddProduct,
  useMinusProduct,
  useRemoveProduct,
} from "../recoil/hooks";
import _ from "lodash";
import helper from "./../services/helper";
import WardSelect from "../components/Select/Wards";
import ProvincesSelect from "../components/Select/Provinces";
import DistrictsSelect from "../components/Select/Districts";
import clientData from "./../services/clientData";
import {useRouter} from "next/router";
// const layout = {
//   labelCol: { span: 3 },
//   wrapperCol: { span: 4 },
// };
const tailLayout = {
  wrapperCol: { offset: 8, span: 16 },
};
const Body = styled.div`
  .add-margin {
    margin-bottom: 10px;
  }
  .product-info {
    img {
      max-width: 5rem;
    }
    .price-product {
      color: #ee4d2d;
    }
    .add-padding {
      padding-top: 25px;
    }
  }
  .total-money {
    text-align: center;
    .price-total {
      font-size: 1.5em;
      color: #ee4d2d;
      margin: 0 15px;
    }
    .button-buy-cart {
      width: 50%;
    }
  }
  .buy-product {
    border-left: solid 1px;
  }
`;
const { Option } = Select;
const Main = (props) => {
  const router = useRouter();
  const [form] = Form.useForm();
  const cardInfo = useRecoilValue(cartStatus);
  let cartItem = cardInfo.item;
  let totalMoney = 0;
  cartItem?.map((e) => {
    totalMoney = totalMoney + e.qty * e.pricePr;
  });
  const addProduct = useAddProduct();
  const minusProduct = useMinusProduct();
  const removeProduct = useRemoveProduct();
  const onFinish = async (values) => {
    if(cartItem.length === 0){
      message.warning("Trong giỏ hàng không có sản phẩm nào.");
      return;
    }
    if (!helper.isLogin()) {
      message.warning("Vui lòng đăng nhập để đặt hàng.");
      return;
    };
    let listProduct = [];
    cartItem.map(e=>{
      listProduct.push({
        id : e.id,
        qty : e.qty
      })
    })
    const data = _.cloneDeep(values);
    data.listProducts = listProduct;
    data.receiveProvince = data.receiveProvince.value;
    data.receiveDistrict = data.receiveDistrict.value;
    data.receiveWard = data.receiveWard.value;
    let createOrder = await clientData.createOrder(data);
    if (createOrder.code === 0) {
      let orderId = createOrder.orderInfo.id;
      let payInfo = await clientData.payOrder(orderId);
      console.log(payInfo)
      if (payInfo.code === 0 && data.methodPayment === "online") {
        window.open(payInfo.vnpUrl, "_blank");
      };
      message.success("Đặt đơn hàng thành công")
      router.push("/user/orders");
    }
  };
  return (
    <>
      <Head />
      <Body>
        <Row justify="center" className="add-margin">
          <Col lg={20} md={24}>
            <Card>
              <Row>
                <Col span={12}>
                  <span>Sản phẩm</span>
                </Col>
                <Col span={3}>Đơn giá</Col>
                <Col span={3}>Số lượng</Col>
                <Col span={3}>Số tiền</Col>
                <Col span={2} offset={1}>
                  Thao tác
                </Col>
              </Row>
            </Card>
          </Col>
        </Row>
        {cartItem?.map((e) => {
          return (
            <Row justify="center" className="add-margin" key={e.id}>
              <Col lg={20} md={24}>
                <Card>
                  <Row className="product-info">
                    <Col span={12}>
                      <img src={e.imagePr}></img>
                      <span>{e.name} - <span style={{fontWeight:"600"}}>{e.attributeName} : {e.attributeValue}</span></span>
                    </Col>
                    <Col span={3} className="add-padding">
                      <span className="price-product">
                        {e.pricePr.toLocaleString()}đ
                      </span>
                    </Col>
                    <Col span={3} className="add-padding">
                      <span className="amount-product">
                        <Button
                          icon={<PlusOutlined />}
                          size="small"
                          onClick={() => {
                            addProduct(e, 1);
                          }}
                        />
                        {e.qty}
                        <Button
                          icon={<MinusOutlined />}
                          size="small"
                          onClick={() => {
                            if (e.qty > 1) minusProduct(e, 1);
                          }}
                        />
                      </span>
                    </Col>
                    <Col span={3} className="add-padding">
                      <span className="amount-money-product price-product">
                        {(e.qty * e.pricePr).toLocaleString()}đ
                      </span>
                    </Col>
                    <Col span={2} offset={1} className="add-padding">
                      <a
                        onClick={() => {
                          removeProduct(e);
                        }}
                      >
                        Xóa
                      </a>
                    </Col>
                  </Row>
                </Card>
              </Col>
            </Row>
          );
        })}
        <Affix offsetBottom={5}>
          <Row justify="center">
            <Col lg={20} md={24} className="total-money">
              <Card>
                <Row>
                  <Col span={18}>
                    <Form
                      // {...layout}
                      form={form}
                      layout="vertical"
                      name="basic"
                      onFinish={onFinish}
                      //   initialValues={{ remember: true }}
                      //   onFinish={onFinish}
                      //   onFinishFailed={onFinishFailed}
                    >
                      <Row>
                        <Col span={7}>
                          <Form.Item
                            label="Tên người nhận"
                            name="receiveName"
                            rules={[
                              {
                                required: true,
                                message: "Vui lòng nhập tên người nhận",
                              },
                            ]}
                          >
                            <Input />
                          </Form.Item>
                        </Col>
                        <Col span={13} offset={1}>
                          <Form.Item
                            label="Địa chỉ"
                            name="receiveAddress"
                            rules={[
                              {
                                required: true,
                                message: "Vui lòng nhập địa chỉ của bạn",
                              },
                            ]}
                          >
                            <Input />
                          </Form.Item>
                        </Col>
                      </Row>

                      <Row>
                        <Col span={5}>
                          <Form.Item
                            label="Tỉnh/Thành phố"
                            name="receiveProvince"
                            rules={[
                              {
                                required: true,
                                message: "Vui lòng chọn tỉnh của bạn",
                              },
                            ]}
                          >
                            <ProvincesSelect mode="simple" />
                          </Form.Item>
                        </Col>
                        <Col span={5} offset={1}>
                          {" "}
                          <Form.Item
                            label="Huyện"
                            name="receiveDistrict"
                            rules={[
                              {
                                required: true,
                                message: "Vui lòng chọn huyện của bạn ",
                              },
                            ]}
                          >
                            <DistrictsSelect mode="simple" />
                          </Form.Item>
                        </Col>

                        <Col span={5} offset={1}>
                          <Form.Item
                            label="Xã"
                            name="receiveWard"
                            rules={[
                              {
                                required: true,
                                message: "Vui lòng chọn xã của bạn",
                              },
                            ]}
                          >
                            <WardSelect mode="simple" />
                          </Form.Item>
                        </Col>
                      </Row>
                      <Row>
                        <Col span={8}>
                          <Form.Item
                            label="Phương thức thanh toán"
                            name="methodPayment"
                            rules={[
                              {
                                required: true,
                                message: "Vui lòng chọn phương thức thanh toán",
                              },
                            ]}
                          >
                            <Select placeholder="Vui lòng chọn phương thức thanh toán">
                              <Option value="cod">
                                Thanh toán khi nhận hàng
                              </Option>
                              <Option value="online">Online</Option>
                            </Select>
                          </Form.Item>
                        </Col>
                        <Col span={12} offset={1}>
                          <Form.Item
                            label="Ghi chú"
                            name="note"
                            rules={[
                              {
                                required: false,
                                message: "Vui lòng nhập địa chỉ của bạn",
                              },
                            ]}
                          >
                            <Input />
                          </Form.Item>
                        </Col>
                      </Row>
                    </Form>
                  </Col>
                  <Col span={6}>
                    <div className="buy-product">
                      <span>Tổng tiền sản phẩm:</span>
                      <span className="price-total">
                        {totalMoney.toLocaleString()}đ
                      </span>
                      <p>
                        <Button
                          type="primary"
                          size="large"
                          className="button-buy-cart"
                          onClick={() => form.submit()}
                        >
                          Mua hàng
                        </Button>
                      </p>
                    </div>
                  </Col>
                </Row>
              </Card>
            </Col>
          </Row>
        </Affix>
      </Body>
      <Footer />
    </>
  );
};
export default Main;
