import { message, Checkbox, Card, Form, Input, Button } from "antd";
import styled from "styled-components";
import Link from "next/link";
import clientData from "./../services/clientData"
const Body = styled.div`
  padding : 14% 0;
  height : 100vh;
  background-image :  url("/images/background.jpg");
  background-size: auto;
  .card-login {
    width: 30%;
    margin: auto;
  }
  .register {
      text-align: center;
  }
  .ant-card-head-title{
      text-align : center
  }
`;
const Main = () => {
  const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 8 },
  };
  const tailLayout = {
    wrapperCol: { offset: 8, span: 8 },
  };
  const onFinish = async (values) => {
    console.log('Success:', values);
    let data = await clientData.regiterCustomer(values);
    if(data.code === 0){
        message.success(data.message)
    }else{
        message.error(data.message)
    }
  };
  return (
    <Body>
      <Card className="card-login" title="Đăng nhập vào tài khoản">
        <Form
          {...layout}
          name="basic"
          initialValues={{ remember: true }}
            onFinish={(val)=>onFinish(val)}
          //   onFinishFailed={onFinishFailed}
        >
          <Form.Item
            label="Số điện thoại:"
            name="phone"
            rules={[{ required: true, message: "Please input your username!" }]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Mật khẩu:"
            name="password"
            rules={[{ required: true, message: "Please input your password!" }]}
          >
            <Input.Password />
          </Form.Item>
          <Form.Item
            label="Nhắc lại mật khẩu:"
            name="confirmPassword"
            rules={[{ required: true, message: "Please input your password!" }]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item {...tailLayout}>
            <Button type="primary" htmlType="submit">
              Đăng ký
            </Button>
          </Form.Item>
          <div className="register">
            <span>Bạn đã có tài khoản? </span>
            <Link href="/login">
              <a>Đăng nhập</a>
            </Link>
          </div>
        </Form>
      </Card>
    </Body>
  );
};
export default Main;
