import { message, Checkbox, Card, Form, Input, Button } from "antd";
import styled from "styled-components";
import Link from "next/link";
import clientData from "./../services/clientData";
import { useRouter } from "next/router";
import {updateCustomerInfo} from "./../recoil/hooks"
const Body = styled.div`
  padding: 14% 0;
  height: 100vh;
  background-image: url("/images/background.jpg");
  background-size: auto;
  .card-login {
    width: 30%;
    margin: auto;
  }
  .register {
    text-align: center;
  }
  .ant-card-head-title {
    text-align: center;
  }
`;
const Main = () => {
  let router = useRouter();
  const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 8 },
  };
  const tailLayout = {
    wrapperCol: { offset: 8, span: 8 },
  };
  const onFinish = async (values) => {
    console.log("Success:", values);
    let data = await clientData.loginByPassword(values);
    if (data.code === 0) {
      message.success("Đăng nhập thành công");
      localStorage.setItem("accessToken", data.accessToken);
      localStorage.setItem("customerInfo", JSON.stringify(data.userInfo));
      router.push("/")
    } else {
      message.error(data.message);
    }
  };
  return (
    <Body>
      <Card className="card-login" title="Đăng nhập vào tài khoản">
        <Form
          {...layout}
          name="basic"
          initialValues={{ remember: true }}
          onFinish={(val) => onFinish(val)}
          //   onFinishFailed={onFinishFailed}
        >
          <Form.Item
            label="Số điện thoại:"
            name="phone"
            rules={[{ required: true, message: "Please input your username!" }]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            label="Mật khẩu:"
            name="password"
            rules={[{ required: true, message: "Please input your password!" }]}
          >
            <Input.Password />
          </Form.Item>

          <Form.Item {...tailLayout}>
            <Button type="primary" htmlType="submit">
              Đăng nhập
            </Button>
          </Form.Item>
          <div className="register">
            <span>Bạn chưa có tài khoản? </span>
            <Link href="/register">
              <a>Đăng ký</a>
            </Link>
          </div>
        </Form>
      </Card>
    </Body>
  );
};
export default Main;
