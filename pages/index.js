import Head from "./../components/Header";
import Footer from "./../components/Footer";
import { Row, Col, Input, Menu, Card, Tabs, Divider } from "antd";
import Slider from "react-slick";
import styled from "styled-components";
import { LeftOutlined, RightOutlined } from "@ant-design/icons";
import { LazyLoadImage } from "react-lazy-load-image-component";
import ListProduct from "./../components/ListProduct";
import Link from "next/link";
const Body = styled.div`
  margin-top: 2px;
  .slider-news {
    .card-news {
      height: 100%;
      margin: 0 5px;
      overflow-y: auto;
      max-height: 330px;
      .ant-card-body {
        padding: 5px;
      }
      .ant-tabs-tab {
        /* width: 110px; */
        justify-content: center;
      }
      .row-product {
        padding: 5px;
        clear: both;
        margin: 12px 0;
        height: 76px;
        position: relative;
        border-bottom: 1px solid #e0e0e0;
        img {
          width: 100%;
          max-height: 70px;
        }
      }
      .title-slide-product {
        color: black;
        overflow: hidden;
        font-size: 1.1em;
        white-space: nowrap;
        text-overflow: ellipsis;
        overflow: hidden;
        display: inherit;
      }
      .category-slide-product {
        color: #909090;
        overflow: hidden;
        display: block;
        font-size: 0.9em;
      }
      .ant-tabs-tab.ant-tabs-tab-active .ant-tabs-tab-btn {
        color: red;
        font-weight: 500;
      }
      .ant-tabs-ink-bar {
        position: absolute;
        background: red;
        pointer-events: none;
      }
    }
    .slick-dots {
      bottom: 0;
    }
  }
  .arrow-prev {
    position: absolute;
    top: 40%;
    z-index: 2;
    opacity: 0.7;
    width: 55px;
    height: 55px;
    border: 0;
    border-radius: 50%;
    text-shadow: none;
    text-align: center;
    -webkit-transition: all ease-in-out 0.3s;
    transition: all ease-in-out 0.3s;
    font-size: 29px;
    background: #fff;
    box-shadow: 0 2px 2px #9a9a9a;
    color: #000;
    display: block !important;
    padding-top: 5px;
  }
  .arrow-next {
    position: absolute;
    right: 0;
    top: 40%;
    z-index: 2;
    opacity: 0.7;
    width: 55px;
    height: 55px;
    border: 0;
    border-radius: 50%;
    text-shadow: none;
    text-align: center;
    -webkit-transition: all ease-in-out 0.3s;
    transition: all ease-in-out 0.3s;
    font-size: 29px;
    background: #fff;
    box-shadow: 0 2px 2px #9a9a9a;
    color: #000;
    display: block !important;
    padding-top: 5px;
  }
  .sanpham {
    margin: 5px 0px;
    .card-sanpham {
      width: 100%;
    }
    .product-style {
      width: 25%;
      text-align: center;
      img {
        width: 100%;
        transition: transform 0.2s;
      }
      img:hover {
        transform: scale(1.1);
      }
    }
  }
`;
const products = [
  {
    id: 1,
    image:
      "https://manager.remaxvietnam.vn/asset/images/SanPham/phukiendienthoai/loabluetooth/RB-M56/av-loa-rb-m56-18012021.png?w=288",
    name: "Phu kien",
    title: "Phu kien dien thoai",
    price: 10000,
  },
  {
    id: 2,
    image:
      "https://manager.remaxvietnam.vn/asset/images/SanPham/phukiendienthoai/loabluetooth/RB-M56/av-loa-rb-m56-18012021.png?w=288",
    name: "Phu kien",
    title: "Phu kien dien thoai",
    price: 10000,
  },
  {
    id: 3,
    image:
      "https://manager.remaxvietnam.vn/asset/images/SanPham/phukiendienthoai/loabluetooth/RB-M56/av-loa-rb-m56-18012021.png?w=288",
    name: "Phu kien",
    title: "Phu kien dien thoai",
    price: 10000,
  },
  {
    id: 4,
    image:
      "https://manager.remaxvietnam.vn/asset/images/SanPham/phukiendienthoai/loabluetooth/RB-M56/av-loa-rb-m56-18012021.png?w=288",
    name: "Phu kien",
    title: "Phu kien dien thoai",
    price: 10000,
  },
  {
    id: 5,
    image:
      "https://manager.remaxvietnam.vn/asset/images/SanPham/phukiendienthoai/loabluetooth/RB-M56/av-loa-rb-m56-18012021.png?w=288",
    name: "Phu kien",
    title: "Phu kien dien thoai",
    price: 10000,
  },
];
function SampleNextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <a
      className="arrow-next"
      // style={{ ...style, display: "block", background: "red" }}
      onClick={onClick}
    >
      <RightOutlined />
    </a>
  );
}

function SamplePrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <a
      className="arrow-prev"
      // style={{ ...style, display: "block", background: "green" }}
      onClick={onClick}
    >
      <LeftOutlined />
    </a>
  );
}
const { TabPane } = Tabs;
export default function Home(props) {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    // arrows: false,
    autoplay: true,
    autoplaySpeed: 3000,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  };
  let {
    bannerHome,
    listProductTypeIsBestBuy,
    listProductTypeIsHot,
    listProductTypeIsNew,
  } = props;
  return (
    <>
      <Head />
      <Body>
        <div className="slider-news">
          <Row justify="center">
            <Col lg={14} md={16}>
              <Slider {...settings}>
                {bannerHome.map((e) => {
                  return <img src={e.image} />;
                })}
              </Slider>
            </Col>
            <Col lg={6} md={8}>
              <Card className="card-news">
                <Tabs defaultActiveKey="1">
                  <TabPane tab="Sản phẩm mới" key="1">
                    {listProductTypeIsNew?.map((e) => {
                      return (
                        <Link href={`/product/${e.id}`}>
                          <a key={e.id}>
                            <Row className="row-product">
                              <Col span={8}>
                                <img src={  e.image[0]}></img>
                              </Col>
                              <Col span={16}>
                                <p className="title-slide-product">{e.name}</p>
                                <p className="category-slide-product">
                                  Đồ chơi công nghệ
                                </p>
                              </Col>
                            </Row>
                          </a>
                        </Link>
                      );
                    })}
                  </TabPane>
                  <TabPane tab="Tin tức" key="2">
                    <a>
                      <Row className="row-product">
                        <Col span={8}>
                          <img src="https://manager.remaxvietnam.vn/asset/images/SanPham/dochoicongnghe/remax-rt-eb05/slide/slide-xe-dien-remax-rt-eb05-2-19112019.png?w=95"></img>
                        </Col>
                        <Col span={16}>
                          <p className="title-slide-product">Xe điện remax</p>
                          <p className="category-slide-product">
                            Đồ chơi công nghệ
                          </p>
                        </Col>
                      </Row>
                    </a>
                  </TabPane>
                </Tabs>
              </Card>
            </Col>
          </Row>
        </div>
        <Row justify="center">
          <ListProduct
            title="Sản phẩm bán chạy"
            products={listProductTypeIsBestBuy}
          />
          <ListProduct title="Sản phẩm hot" products={listProductTypeIsHot} />
          <ListProduct title="Sản phẩm đang giảm giá" products={listProductTypeIsBestBuy} />
          <ListProduct
            title="Sản phẩm bán mới về"
            products={listProductTypeIsNew}
          />
        </Row>
      </Body>
      <Footer />
    </>
  );
}

export async function getStaticProps() {
  const serverData = require("../services/serverData");
  let data = await serverData.getAllBanners();
  let getListIsBestBuy = await serverData.getListProductTypes({
    skip: 0,
    limit: 10,
    isBestBuy: 1,
  });
  let getListIsHot = await serverData.getListProductTypes({
    skip: 0,
    limit: 10,
    isHot: 1,
  });
  let getListIsNew = await serverData.getListProductTypes({
    skip: 0,
    limit: 10,
    isNew: 1,
  });
  // let dataConfig = await serverData.getConfig();
  let bannerHome = data?.data || [];
  getListIsBestBuy = getListIsBestBuy?.data || [];
  getListIsHot = getListIsHot?.data || [];
  getListIsNew = getListIsNew?.data || [];
  bannerHome.sort(function (a, b) {
    return b.sequence - a.sequence;
  });
  return {
    props: {
      bannerHome,
      listProductTypeIsBestBuy: getListIsBestBuy,
      listProductTypeIsHot: getListIsHot,
      listProductTypeIsNew: getListIsNew,
    },
    revalidate: 10,
  };
}
