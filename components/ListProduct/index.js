import React from "react";
import { Row, Col, Input, Menu, Card, Tabs, Divider, Button } from "antd";
import Slider from "react-slick";
import styled from "styled-components";
import { LeftOutlined, RightOutlined } from "@ant-design/icons";
import { LazyLoadImage } from "react-lazy-load-image-component";
import Link from "next/link";
const Body = styled(Col)`
  margin-bottom: 10px;
  .arrow-prev {
    position: absolute;
    top: 40%;
    z-index: 2;
    opacity: 0.7;
    width: 55px;
    height: 55px;
    border: 0;
    border-radius: 50%;
    text-shadow: none;
    text-align: center;
    -webkit-transition: all ease-in-out 0.3s;
    transition: all ease-in-out 0.3s;
    font-size: 29px;
    background: #fff;
    box-shadow: 0 2px 2px #9a9a9a;
    color: #000;
    display: block !important;
    padding-top: 5px;
  }
  .arrow-next {
    position: absolute;
    right: 0;
    top: 40%;
    z-index: 2;
    opacity: 0.7;
    width: 55px;
    height: 55px;
    border: 0;
    border-radius: 50%;
    text-shadow: none;
    text-align: center;
    -webkit-transition: all ease-in-out 0.3s;
    transition: all ease-in-out 0.3s;
    font-size: 29px;
    background: #fff;
    box-shadow: 0 2px 2px #9a9a9a;
    color: #000;
    display: block !important;
    padding-top: 5px;
  }
  .product {
    border: 1px solid #ececec;
    border-radius: 3px;
    position: relative;
    padding-right: 5px;
    padding-left: 5px;
    text-align: center;
    img {
      width: 100%;
      transition: transform 0.2s;
      /* max-height : 189.18px */
    }
    img:hover {
      transform: scale(1.1);
    }
    h4 {
      color: #1e1e1e;
      font-size: 15px !important;
      line-height: 18px;
      text-decoration: none;
      text-shadow: none !important;
      text-overflow: ellipsis;
      overflow: hidden;
      white-space: nowrap;
    }
    .product-price {
      color: red;
    }
    .bt-buy {
      margin-bottom: 5px;
    }
    .price {
      color: black;
    }
  }
`;
function SampleNextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <a
      className="arrow-next"
      // style={{ ...style, display: "block", background: "red" }}
      onClick={onClick}
    >
      <RightOutlined />
    </a>
  );
}

function SamplePrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <a
      className="arrow-prev"
      // style={{ ...style, display: "block", background: "green" }}
      onClick={onClick}
    >
      <LeftOutlined />
    </a>
  );
}
const optionSlider = {
  dots: false,
  infinite: false,
  speed: 500,
  slidesToShow: 4,
  slidesToScroll: 4,
  // arrows: false,
  nextArrow: <SampleNextArrow />,
  prevArrow: <SamplePrevArrow />,
};
const Main = (props) => {
  const { title, products = [] } = props;
  return (
    <Body lg={20} md={24}>
      <Card title={title}>
        <Slider {...optionSlider}>
          {products.map((e) => {
            return (
              <div className="product">
                <Link href={`/product/${e.id}`}>
                  <a key={e.id}>
                    <LazyLoadImage
                      src={e.image[0]}
                      alt="Sản phẩm"
                      effect="opacity"
                      className="img-product"
                    ></LazyLoadImage>
                    <h4 className="product-name">{e.name}</h4>
                    <p>
                      <span className="price"> Giá:</span>{" "}
                      <span className="product-price">
                        {e.price.toLocaleString()}đ
                      </span>
                    </p>
                    <Button type="primary" className="bt-buy" size="large">
                      Mua ngay
                    </Button>
                  </a>
                </Link>
              </div>
            );
          })}
        </Slider>
      </Card>
    </Body>
  );
};
export default Main;
