import {
  Row,
  Col,
  Input,
  Menu,
  Badge,
  Affix,
  Dropdown,
  Card,
  Avatar,
} from "antd";
import styled from "styled-components";
import {
  MenuOutlined,
  ShoppingCartOutlined,
  PhoneOutlined,
  BellOutlined,
  UserOutlined,
  LogoutOutlined,
  OrderedListOutlined,
} from "@ant-design/icons";
import helper from "./../../services/helper";
import { useRecoilValue } from "recoil";
import { cartStatus, customerInfomation } from "../../recoil/Selector";
import Link from "next/link";
import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import clientData from "./../../services/clientData"
const { Search } = Input;
const Header = styled.div`
  background: white;
  padding: 12px;
  margin-bottom: 15px;
  img {
    padding-top: 8px;
  }
  .menu {
    border-bottom: none;
  }
  .cart-col {
    text-align: center;
  }
  .cart-icon {
    font-size: 30px;
    /* color: yellow; */
    text-align: center;
  }
  .tel-co-col {
    text-align: center;
    padding-top: 10px;
    .padding {
      padding-left: 5px;
      padding-right: 5px;
    }
    .padding:hover {
      color: red;
    }
    .border {
      border-left: solid 1px red;
    }
    .phone-customer {
      margin-left: 5px;
    }
  }
`;
const MenuNav = styled.div`
  .menu {
    background: #fed700;
    .anticon {
      padding-top: 7px;
      font-size: 20px;
      display: block;
      padding-bottom: 2px;
    }
  }
`;
const { SubMenu } = Menu;
export default function Head(props) {
  let router = useRouter();
  // let { categories } = props;
  const [customerInfo, setcustomerInfo] = useState();
  const [categories, setcategories] = useState([]);
  useEffect(() => {
    if (helper.isLogin()) {
      let temp = localStorage.getItem("customerInfo");
      try {
        temp = JSON.parse(temp);
      } catch (error) {
        temp = {};
      }
      setcustomerInfo(temp);
    }
    getData()
  }, []);
  const getData = async () => {
    let data = await clientData.getCategories();
    console.log(data);
    let temp = data.data;
    setcategories(temp)

  };
  const cartInfo = useRecoilValue(cartStatus);
  // const customerInfo = useRecoilValue(customerInfomation);
  const CardDropdown = () => {
    return <Card className="card-dropdown">Bạn chưa có thông báo nào!</Card>;
  };
  const logout = () => {
    localStorage.clear();
    router.reload();
  };
  const menu = (
    <Menu>
      <Menu.Item icon={<UserOutlined />}>
        <Link href="/user/profile">
          <a>Thông tin cá nhân</a>
        </Link>
      </Menu.Item>
      <Menu.Item icon={<OrderedListOutlined />}>
        <Link href="/user/orders">
          <a>Đơn hàng của bạn</a>
        </Link>
      </Menu.Item>
      <Menu.Item icon={<LogoutOutlined />}>
        <a
          onClick={() => {
            logout();
          }}
        >
          Đăng xuất
        </a>
      </Menu.Item>
    </Menu>
  );
  return (
    <Affix offsetTop={0}>
      <Header>
        <Row justify="center">
          <Col span={2}>
            <Link href="/">
              <a>
                <img
                  className="logo"
                  src="https://remaxvietnam.vn/images/icon/logo_remax.svg"
                ></img>
              </a>
            </Link>
          </Col>
          <Col span={3}>
            <Menu mode="horizontal" className="menu">
              <SubMenu icon={<MenuOutlined />} title="Danh mục">
                {categories.map(e=>{
                  return <Menu.Item>{e.name}</Menu.Item>
                })}

                {/* <Menu.Item>Phụ kiện xe máy</Menu.Item>
                <Menu.Item>Phụ kiện gia đình</Menu.Item>
                <Menu.Item>Phụ kiện điện thoại</Menu.Item>
                <Menu.Item>Phụ kiện Laptop</Menu.Item>
                <Menu.Item>Thời trang</Menu.Item> */}
              </SubMenu>
            </Menu>
          </Col>
          <Col span={9} style={{ marginTop: "10px" }}>
            <Search
              placeholder="Tìm kiếm voucher, cửa hàng"
              enterButton
              size="large"
              className="seachbutton"
              size="middle"
            />
          </Col>
          <Col span={1} style={{ marginTop: "10px" }} className="cart-col">
            <Badge count={cartInfo.itemLength} showZero>
              <Link href="/cart">
                <a>
                  <ShoppingCartOutlined className="cart-icon" />
                </a>
              </Link>
            </Badge>
          </Col>
          <Col span={1} style={{ marginTop: "10px" }} className="cart-col">
            <Dropdown placement="bottomRight" arrow overlay={CardDropdown}>
              <Badge count={0} showZero>
                <a>
                  <BellOutlined className="cart-icon" />
                </a>
              </Badge>
            </Dropdown>
          </Col>
          {customerInfo ? (
            <Col span={3} className="tel-co-col">
              <Dropdown overlay={menu} arrow placement="bottomCenter">
                <div>
                  {customerInfo?.image ? (
                    <Avatar src={customerInfo.image}></Avatar>
                  ) : (
                    <Avatar icon={<UserOutlined />}></Avatar>
                  )}
                  <span className="phone-customer">{customerInfo.phone}</span>
                </div>
              </Dropdown>
            </Col>
          ) : (
            <Col span={3} className="tel-co-col">
              {/* <PhoneOutlined />
            <span className="tel-co">&nbsp;19008198</span> */}
              <Link href="/login">
                <a className="login-button padding ">Đăng nhập</a>
              </Link>
              <Link href="/register">
                <a className="signin-button padding border">Đăng ký</a>
              </Link>
            </Col>
          )}
        </Row>
      </Header>
    </Affix>
  );
}
export async function getStaticProps() {
  const clientData = require("../../services/clientData");
  let data = await clientData.getCategories();
  console.log(data);
  let categories = data.data;
  return {
    props: {
      categories,
    },
    revalidate: 1,
  };
}
