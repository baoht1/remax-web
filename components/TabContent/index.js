import styled from "styled-components";
import {
  Row,
  Col,
  Card,
  Form,
  Input,
  Button,
  Checkbox,
  DatePicker,
  Radio,
  Select,
  Avatar,
  Tabs,
  Image,
} from "antd";
import { useState, useEffect } from "react";
import moment from "moment";
import { DollarOutlined } from "@ant-design/icons";
import clientData from "./../../services/clientData";
const Body = styled.div`
  margin-bottom: 1em;
  .card-content-order {
    margin-bottom: 1em;
  }
  .content-header {
    .left-header-content {
      float: left;
      .hour-createdAt {
        font-weight: 600;
        font-size: 1.1em;
        margin-left: 5px;
      }
    }
    .right-header-content {
      float: right;
      .status-info {
        font-size: 1.2em;
        font-weight: 600;
        color: #ee4d2d;
        margin-left: 7px;
      }
    }
  }
  .product-item {
    .product-content {
      .left-content-product {
        width: 75%;
        float: left;
        img {
          width: 5rem;
          height: 5rem;
          margin-right: 7px;
        }
        .left {
          float: left;
        }
      }
      p {
        margin-bottom: 0.5em;
        overflow: hidden;
      }
      .name-product {
        font-weight: 600;
        font-size: 1.4em;
      }
      .right-content-product {
        float: right;
        text-align: right;
        /* margin-top : 30%; */
        .price {
          font-size: 1.2em;
          color: #ee4d2d;
        }
      }
    }
  }
  .content-footer {
    .price {
      font-size: 1.2em;
      color: #ee4d2d;
    }
    .payment-status {
      font-weight: 600;
      font-size: 1.2em;
      margin: 0 5px;
    }
    text-align: right;
    .total-money {
      font-size: 1.875em;
      color: #ee4d2d;
      margin-left: 10px;
    }
    .btn-info-check:hover {
      color: #ee4d2d;
      border-color: #ee4d2d;
    }
  }
`;
const CardNoOrder = styled(Card)`
  width: 100%;
  height: 37.5rem;
  padding: 10% 42%;
  .empty-order-icon {
    background-position: 50%;
    background-size: cover;
    background-repeat: no-repeat;
    background-image: url(https://deo.shopeemobile.com/shopee/shopee-pcmall-live-sg/assets/50ae7a4bf7cca69985b40dfea02eddb3.png);
    width: 6.25rem;
    height: 6.25rem;
    margin-bottom: 1.25rem;
  }
`;
const Divider = styled.div`
  border-bottom: solid 1px rgba(0, 0, 0, 0.09);
  padding: 5px 0;
  clear: both;
  margin-bottom: 5px;
`;
const mapStatusOrder = {
  wait_for_confirm: "Chờ xác nhận",
};
const mapstatusPayment = {
  pending: "Đang đợi",
  processing: "Đang tiến hành",
  success: "Thành công",
};
const mapStatusKey = {
  2: "wait_for_confirm",
  3: "wait_for_take_order",
  4: "shipping",
  5: "success",
  6: "cancel",
};
const Main = (props) => {
  const [orderInfoKey, setOrderInfoKey] = useState({});
  const [loading, setloading] = useState(false);
  let { keyOrder } = props;
  useEffect(() => {
    getData(keyOrder);
  }, []);
  const getData = async (keyOrder) => {
    setloading(true);
    let data;
    if (keyOrder === 1) {
      data = await clientData.getListOrder();
    } else {
      data = await clientData.getListOrder(mapStatusKey[Number(keyOrder)]);
    }
    let temp = Object.assign(orderInfoKey);
    temp[Number(keyOrder)] = data.data;
    console.log(temp);
    setOrderInfoKey(temp);
    setloading(false);
  };
  if (loading) {
    <CardNoOrder>
      <div className="container">
        <span>Đang tải...</span>
      </div>
    </CardNoOrder>;
  }
  if (
    !orderInfoKey[Number(keyOrder)] ||
    orderInfoKey[Number(keyOrder)].length === 0
  )
    return (
      <CardNoOrder>
        <div className="container">
          <div className="empty-order-icon"></div>
          <div className="empty-order-text"> Chưa có đơn hàng</div>
        </div>
      </CardNoOrder>
    );
  return (
    <Body>
      {Array.isArray(orderInfoKey[Number(keyOrder)])
        ? orderInfoKey[Number(keyOrder)].map((e) => {
            return (
              <Card className="card-content-order">
                <div className="content-header">
                  <div className="left-header-content">
                    <span className="hour-title">Ngày đặt hàng:</span>
                    <span className="hour-createdAt">
                      {moment(e.createdAt).format("DD-MM-YYYY hh:mm")}
                    </span>
                  </div>
                  <div className="right-header-content">
                    <span className="status-title">Trạng thái:</span>
                    <span className="status-info">
                      {mapStatusOrder[e.status] || e.status}
                    </span>
                  </div>
                </div>
                <Divider />
                <div className="product-item">
                  {e.orderItems.map((item) => {
                    return (
                      <div className="product-content">
                        <span className="left-content-product">
                          <div className="left">
                            <Image src={item.images} className="image-item" />
                          </div>
                          <div className="right">
                            <p className="name-product">{item.name}</p>
                            <span>Phân loại hàng:</span>
                            <span>
                              {item.attributeName} {item.attributeValue}
                            </span>
                            <p>
                              <span>Số lượng: </span>
                              <span>x{item.quantity}</span>
                            </p>
                          </div>
                        </span>
                        <span className="right-content-product">
                          <span className="price">
                            đ{item.price.toLocaleString()}
                          </span>
                        </span>
                        <Divider />
                      </div>
                    );
                  })}
                </div>
                <div className="content-footer">
                  <div>
                    <span>Tiền ship hàng: </span>
                    <span className="price">20.000đ</span>
                  </div>
                  <div>
                    <span>Trạng thái thanh toán:</span>
                    <span className="payment-status">
                      {mapstatusPayment[e.paymentStatus]}
                    </span>
                    <DollarOutlined
                      style={{
                        width: "30px",
                        height: "30px",
                        fontSize: "1.5em",
                        color: "red",
                      }}
                    />
                    <span>Tổng tiền:</span>
                    <span className="total-money">
                      {e.totalMoney.toLocaleString()}đ{" "}
                    </span>
                  </div>
                  <div>
                    <Button size="large" className="btn-info-check">
                      Xem chi tiết đơn hàng
                    </Button>
                  </div>
                </div>
              </Card>
            );
          })
        : ""}
    </Body>
  );
};
export default Main;
