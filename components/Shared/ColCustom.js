import { Col } from "antd";
const Main = (props) => {
  return (
    <Col lg={20} md={24}>
      {props.children}
    </Col>
  );
};
export default Main;
