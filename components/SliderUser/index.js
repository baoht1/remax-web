import { Col, Avatar, Menu } from "antd";
import styled from "styled-components";
import {
  MenuOutlined,
  EditOutlined,
  UserOutlined,
  MailOutlined,
  AppstoreOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import { useEffect, useState} from "react";

import { useRouter } from "next/router";
const ColCp = styled(Col)`
  .user-info {
    display: block;
    padding: 10px 0;
    /* border-bottom : solid 1px; */
    /* height: 50%; */
    .avt {
      float: left;
      width: 25%;
    }
    .user-name {
      float: right;
      width: 65%;
      /* text-align : center; */
      p {
        margin-bottom: 0.5em;
      }
    }
    .name {
      font-weight: 600;
    }
  }
  .menu-slider {
    background: none;
    .ant-menu-item-selected {
      color: red;
      background: none;
    }
    .ant-menu-item-active {
      color: red;
    }
    .ant-menu-item::after {
      border-right: none;
    }
    .ant-menu-item {
      padding-left: 0px !important;
    }
  }
  .divider-us {
    clear: both;
    border-bottom: solid 1px #efefef;
    padding: 10px 0;
  }
`;
const Main = (props) => {
  let router = useRouter();
  let { path } = props;
  const [custoemrInfo, setcustoemrInfo] = useState({});
  const menuOnclick = (e) => {
    router.push(e.key);
  };
  useEffect(() => {
    let userInfo = localStorage.getItem("customerInfo");
    userInfo = JSON.parse(userInfo)
    console.log(userInfo)
    setcustoemrInfo(userInfo)
  }, []);
  return (
    <ColCp span={4}>
      <div className="user-info">
        <Avatar className="avt" icon={<UserOutlined />} size={60}></Avatar>
        <span className="user-name">
          <p className="name">{custoemrInfo?.phone}</p>
          <a className="edit-bt" href="/user/profile">
            <EditOutlined />
            Sửa hồ sơ
          </a>
        </span>
      </div>
      <div className="divider-us"></div>
      <div className="sider-bar">
        <Menu
          onClick={(e) => menuOnclick(e)}
          style={{ width: 256 }}
          defaultSelectedKeys={[path]}
          mode="inline"
          className="menu-slider"
        >
          <Menu.Item icon={<UserOutlined />} key={"/user/profile"}>
            Thông tin cá nhân
          </Menu.Item>

          <Menu.Item icon={<SettingOutlined />} key={"/user/orders"}>
            Đơn hàng
          </Menu.Item>
        </Menu>
      </div>
    </ColCp>
  );
};
export default Main;
