import { Row, Col, Card } from "antd";
import { CheckCircleTwoTone } from "@ant-design/icons";
import ColCustom from "./../Shared/ColCustom"
const Main = () => {
  return (
    <Row justify="center">
      <ColCustom span={20}> 
        <Card>
          <Row>
            <Col span={6}>
              <span>
                <CheckCircleTwoTone
                  style={{
                    fontSize: "50px",
                    color: "#08c",
                    float: "left",
                    paddingRight: "10px",
                  }}
                />
              </span>
              <span>Chính hãng</span>
              <div>Sản phẩm chính hãng</div>
            </Col>
            <Col span={6}>
              <span>
                <CheckCircleTwoTone
                  style={{
                    fontSize: "50px",
                    color: "#08c",
                    float: "left",
                    paddingRight: "10px",
                  }}
                />
              </span>
              <span>Chính hãng</span>
              <div>Sản phẩm chính hãng</div>
            </Col>
            <Col span={6}>
              <span>
                <CheckCircleTwoTone
                  style={{
                    fontSize: "50px",
                    color: "#08c",
                    float: "left",
                    paddingRight: "10px",
                  }}
                />
              </span>
              <span>Chính hãng</span>
              <div>Sản phẩm chính hãng</div>
            </Col>
            <Col span={6}>
              <span>
                <CheckCircleTwoTone
                  style={{
                    fontSize: "50px",
                    color: "#08c",
                    float: "left",
                    paddingRight: "10px",
                  }}
                />
              </span>
              <span>Chính hãng</span>
              <div>Sản phẩm chính hãng</div>
            </Col>
          </Row>
        </Card>
      </ColCustom>
      <ColCustom span={20}>
        <Card>
          <div className="container bg-chinhsach">
            <div className="wapcontainer">
              <div className="row footer-bottom">
                <div className="col-md-12">
                  <h4>Bản quyền © Cty cổ phần Học viện Kỹ Thuật Quân sự</h4>
                  <p>
                    Công ty Cổ Phần Học viện Kỹ Thuật Quân sự Giấy phép kinh doanh số
                    0107875808 do sở kế hoạch và đầu tư thành phố Hà Nội cấp
                    ngày 06/06/2017
                  </p>
                  <p>
                    Văn phòng Hà Nội: Số 298 phố Tây Sơn, P. Ngã Tư Sở, Q. Đống
                    đa, Tp. Hà Nội
                  </p>
                  <p>Hotline :1900 2226 | Email: cskh@remaxvietnam.vn</p>
                </div>
              </div>
            </div>
          </div>
        </Card>
      </ColCustom>
    </Row>
  );
};
export default Main;
