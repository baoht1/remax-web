const request = require("request");
const NodeCache = require("node-cache");
const cache = new NodeCache();
let methods = {};
const callRequest = async (options) => {
  try {
    let key = "",
      data = null;
    if (options.useCache) {
      key = `${options.url}_${options.body}`;
      data = cache.get(key);
      if (data) return data;
    }
    data = await new Promise((resolve, reject) => {
      request(options, function (error, response) {
        try {
          if (error) {
            throw new Error(error);
          }
          resolve(JSON.parse(response.body));
        } catch (error) {
          reject(null);
        }
      });
    });
    if (options.useCache) {
      cache.set(key, data, 10);
    }
    return data;
  } catch (error) {
    console.log(error);
    return null;
  }
};
methods.shuffleArray = function (arr) {
  let tmp = arr.map((i) => {
    i._factor = Math.random();
    return i;
  });

  tmp.sort((a, b) => a._factor - b._factor);
  return tmp;
};
methods.getAllBanners = async () => {
  let raw = await callRequest({
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    url: `${process.env.API_HOST}/api/banner/get-all-banners`,
    body: JSON.stringify({}),
  });
  return raw;
};

methods.getCustomerInfo = async (token) => {
  let options = {
    headers: {
      Authorization: `Bearer ${token}`,
    },
    method: "POST",
    url: `${process.env.API_HOST}/api/customer/get-customer-info`,
    body: JSON.stringify({}),
  };
  let rs = await callRequest(options);
  return rs;
};
methods.getCategories = async () => {
  let options = {
    useCache: true,
    method: "POST",
    url: `${process.env.API_HOST}/api/category/get`,
    body: JSON.stringify({}),
  };
  let rs = await callRequest(options);
  return rs;
};
methods.getListVouchers = async (input) => {
  let options = {
    method: "POST",
    url: `${process.env.API_HOST}/api/voucher/get-list-vouchers`,
    body: JSON.stringify(input),
  };
  let rs = await callRequest(options);
  return rs;
};
methods.getListProductTypes = async (input) => {
  let options = {
    method: "POST",
    url: `${process.env.API_HOST}/api/product-type/get-list-product-type`,
    body: JSON.stringify(input),
  };
  let rs = await callRequest(options);
  return rs;
};

methods.getVoucherInfo = async (input) => {
  let options = {
    useCache: true,
    method: "POST",
    url: `${process.env.API_HOST}/api/voucher/get-voucher-info`,
    body: JSON.stringify(input),
  };
  let rs = await callRequest(options);
  return rs;
};
methods.getProductTypeInfo = async (productTypeId) => {
  let options = {
    useCache: true,
    method: "POST",
    url: `${process.env.API_HOST}/api/product-type/get-product-type-info`,
    body: JSON.stringify({ productTypeId }),
  };
  let rs = await callRequest(options);
  return rs;
};
methods.getItemInfo = async (itemId) => {
  let options = {
    useCache: true,
    method: "POST",
    url: `${process.env.API_HOST}/api/item/get-item-info`,
    body: JSON.stringify({ itemId }),
  };
  let rs = await callRequest(options);
  return rs;
};
methods.getListComments = async (itemId, skip, limit) => {
  let options = {
    useCache: true,
    method: "POST",
    url: `${process.env.API_HOST}/api/comment/get-list-comments`,
    body: JSON.stringify({ itemId, skip, limit }),
  };
  let rs = await callRequest(options);
  return rs;
};
methods.getListFlashSales = async () => {
  let options = {
    useCache: true,
    method: "POST",
    url: `${process.env.API_HOST}/api/flashsale/get-list-flashsales`,
    body: JSON.stringify({}),
  };
  let rs = await callRequest(options);
  return rs;
};
methods.getShareItemUrl = async (type, typeId) => {
  let options = {
    useCache: true,
    method: "POST",
    url: `${process.env.API_HOST}/api/share/get-share-item-url`,
    body: JSON.stringify({ type, typeId }),
  };
  let rs = await callRequest(options);
  return rs;
};
methods.getVouchers = async (params) => {
  let options = {
    useCache: true,
    method: "POST",
    url: `${process.env.API_HOST}/api/voucher/get-list-vouchers`,
    body: JSON.stringify(params),
  };
  let rs = await callRequest(options);
  return rs;
};
methods.getPartnerHot = async () => {
  let options = {
    useCache: true,
    method: "POST",
    url: `${process.env.API_HOST}/api/partner/get-list-hot-partners`,
    body: JSON.stringify({}),
  };
  let rs = await callRequest(options);
  return rs;
};
methods.getFaq = async () => {
  let options = {
    useCache: true,
    method: "POST",
    url: `${process.env.API_HOST}/api/qa/get-list-qa`,
    body: JSON.stringify({}),
  };
  let rs = await callRequest(options);
  return rs;
};
methods.getPartners = async () => {
  let options = {
    useCache: true,
    method: "POST",
    body: JSON.stringify({}),
    url: `${process.env.API_HOST}/api/partner/get-loyalty-partner`,
  };
  let rs = await callRequest(options);
  return rs;
};

methods.getConfig = async () => {
  let options = {
    useCache: true,
    method: "POST",
    body: JSON.stringify({}),
    url: `${process.env.API_HOST}/api/admin/get-config`,
  };
  let rs = await callRequest(options);
  return rs;
};
methods.getListCampaigns = async () => {
  let options = {
    useCache: true,
    method: "POST",
    body: JSON.stringify({}),
    url: `${process.env.API_HOST}/api/campaign/get-list-campaigns`,
  };
  let rs = await callRequest(options);
  return rs;
};
methods.getListMember = async () => {
  let options = {
    useCache: true,
    method: "POST",
    body: JSON.stringify({}),
    url: `${process.env.API_HOST}/api/member/get-list-members`,
  };
  let rs = await callRequest(options);
  return rs;
};
methods.getProductTypeInfo = async (productTypeId) => {
  let options = {
    useCache: true,
    method: "POST",
    body: JSON.stringify({productTypeId}),
    url: `${process.env.API_HOST}/api/member/get-list-members`,
  };
  let rs = await callRequest(options);
  return rs;
};


module.exports = methods;
