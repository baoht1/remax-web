let helper = {};
helper.isLogin = ()=>{
    let accessToken = localStorage.getItem("accessToken");
    if(accessToken){
        return true;
    };
    return false;
}
helper.fnKhongDau = (str) => {
    let strReturn = str
    strReturn = strReturn.toLowerCase();
    strReturn = strReturn.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    strReturn = strReturn.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    strReturn = strReturn.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    strReturn = strReturn.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    strReturn = strReturn.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    strReturn = strReturn.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    strReturn = strReturn.replace(/đ/g, "d");
    // str = str.replace(/!|@|\$|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\'| |\"|\&|\#|\[|\]|~/g, "-");
    // str = str.replace(/-+-/g, " ");
    // str = str.replace(/^\-+|\-+$/g, "");
    // str = str.replace('-', ' ');
    return strReturn;
  }
export default helper;