import nextConfig from "next/config";
import PubSub from "pubsub-js";
const { publicRuntimeConfig } = nextConfig();
let cache = {};
let methods = {};
const host = "http://thaibao229.tk"
methods.convertStringUrl = (str) => {
  if (!str) return str;
  str = str
    .normalize("NFD")
    .replace(/\%/g, "")
    .replace(/\//g, "")
    .replace(/\\/g, "")
    .replace(/[\u0300-\u036f]/g, "")
    .replace(/đ/g, "d")
    .replace(/Đ/g, "D");
  str = str.toLowerCase();
  str = str.replace(/\ /g, "-");
  return str;
};
const callRequest = async (options) => {
  let url = options.url;
  delete options.url;
  return methods.fetch(url, options);
};

methods.fetch = async (url, options, useCache) => {
  if (useCache) {
    if (cache[url]) {
      return cache[url];
    }
  }
  try {
    if (!options.headers) {
      options.headers = {
        "Content-Type": "application/json; charset=UTF-8",
        Authorization: `Bearer ${localStorage.getItem("accessToken")}`,
        "content-language": localStorage.getItem("language") || "en",
      };
    }

    console.log("--------- POST------------", url, options);

    let res = await fetch(url, options);
    if (res.status == 401 || res.status == 403) {
      PubSub.publish("openLoginRequire", null);
      localStorage.clear();
      throw "Vui lòng đăng nhập";
    }
    // return (window.location.href = "/login/loginWithPhone");

    let rs = await res.json();

    console.log("----------RESPONSE POST------------", url, rs);
    if (useCache) {
      cache[url] = rs;
    }
    return rs;
  } catch (error) {
    // alert(JSON.stringify(error));
    throw error;
  }
};
methods.getCustomerInfo = async () => {
  try {
    let options = {
      method: "POST",
      body: JSON.stringify({}),
    };
    let rs = await methods.fetch(
      `${host}/api/customer/get-customer-info`,
      options
    );
    return rs;
  } catch (error) {
    return {};
  }
};
methods.getFlagCache = () => {
  let rs = cache[`${host}/api/flag/get-list-flags`];
  let obj = {};
  rs.data.map((i) => {
    obj[i.id] = i;
  });
  return obj;
};
methods.getListFlags = async () => {
  let options = {
    method: "POST",
    body: JSON.stringify({}),
  };
  let rs = await methods.fetch(
    `${host}/api/flag/get-list-flags`,
    options,
    true
  );
  let obj = {};
  rs.data.map((i) => {
    obj[i.id] = i;
  });
  return obj;
};
methods.loadCustomerInfo = () => {
  try {
    let rs = localStorage.getItem("customerInfo");
    return JSON.parse(rs);
  } catch (error) {
    localStorage.clear();
    throw "invalid_customer_info";
  }
};
methods.getAvatar = () => {
  let token = localStorage.getItem("accessToken");
  if (!token) return "/images/avatar.png";
  let userInfo = methods.loadCustomerInfo();
  if (userInfo.avatar) return userInfo.avatar;
  if (!userInfo.gender) return "/images/avatar.png";
  return `/images/avatar_${userInfo.gender || "male"}.jpg`;
};
methods.getPaymentCode = async () => {
  return { code: 0, paymentCode: new Date().getTime() };
};
methods.getListCards = async () => {
  let options = {
    method: "POST",
    body: JSON.stringify({}),
  };
  let rs = await methods.fetch(
    `${host}/api/card/get-list-cards`,
    options
  );
  return rs;
};
methods.putInviteCode = async (inviteCode) => {
  let options = {
    method: "POST",
    body: JSON.stringify({ inviteCode }),
  };
  let rs = await methods.fetch(
    `${host}/api/customer/put-invite-code`,
    options
  );
  return rs;
};

methods.getActivePopup = async () => {
  let options = {
    method: "POST",
    body: JSON.stringify({}),
  };
  let rs = await methods.fetch(
    `${host}/api/popup/get-active-popup`,
    options
  );
  return rs;
};
methods.getCardPoint = async (cardId) => {
  let options = {
    method: "POST",
    body: JSON.stringify({ cardId }),
  };
  let rs = await methods.fetch(
    `${host}/api/card/get-card-point`,
    options
  );
  return rs;
};
methods.readAllNoti = async () => {
  let options = {
    method: "POST",
    body: JSON.stringify({}),
  };
  let rs = await methods.fetch(
    `${host}/api/notification/update-all-is-read`,
    options
  );
  return rs;
};
methods.convertPoint = async (cardId, point) => {
  let options = {
    method: "POST",
    body: JSON.stringify({ cardId, point }),
  };
  let rs = await methods.fetch(
    `${host}/api/card/convert-point`,
    options
  );
  return rs;
};
methods.getListProductsForNewUser = async () => {
  let options = {
    method: "POST",
    body: JSON.stringify({}),
  };
  let rs = await methods.fetch(
    `${host}/api/program/get-list-products-for-new-user`,
    options
  );
  return rs;
};
methods.getListMembers = async () => {
  let options = {
    method: "POST",
    body: JSON.stringify({}),
  };
  let rs = await methods.fetch(
    `${host}/api/member/get-list-members`,
    options
  );
  return rs;
};
methods.addATZCard = async () => {
  let options = {
    method: "POST",
    body: JSON.stringify({}),
  };
  let rs = await methods.fetch(
    `${host}/api/card/add-atz-card`,
    options
  );
  return rs;
};

methods.getListVouchers = async (input) => {
  let options = {
    method: "POST",
    body: JSON.stringify(input),
  };
  let rs = await methods.fetch(
    `${host}/api/voucher/get-list-vouchers`,
    options
  );
  return rs;
};
methods.useCodePoint = async (code) => {
  let options = {
    method: "POST",
    body: JSON.stringify({ code }),
  };
  let rs = await methods.fetch(
    `${host}/api/codepoint/use-code-point`,
    options
  );
  return rs;
};

methods.getDiscountInfo = async (discountCode) => {
  let options = {
    method: "POST",
    body: JSON.stringify({ discountCode }),
  };
  let rs = await methods.fetch(
    `${host}/api/discount/get-discount-info`,
    options
  );
  return rs;
};
methods.uploadCommentImage = async (image) => {
  let options = {
    method: "POST",
    body: JSON.stringify({ image }),
  };
  let rs = await methods.fetch(
    `${host}/api/comment/upload-comment-image`,
    options
  );
  return rs;
};
methods.getListProductTypes = async (input) => {
  let options = {
    method: "POST",
    body: JSON.stringify(input),
  };
  let rs = await methods.fetch(
    `${host}/api/product-type/get-list-product-types`,
    options
  );
  return rs;
};
methods.registerSale = async (data) => {
  let options = {
    method: "POST",
    body: JSON.stringify(data),
  };
  let rs = await methods.fetch(
    `${host}/api/shop/register-sale`,
    options
  );
  return rs;
};


methods.ratingProduct = async (input) => {
  let options = {
    method: "POST",
    body: JSON.stringify(input),
  };
  let rs = await methods.fetch(
    `${host}/api/rate/rating-product`,
    options
  );
  return rs;
};
methods.getListComments = async (itemId, skip, limit) => {
  let options = {
    method: "POST",
    body: JSON.stringify({ itemId, skip, limit }),
  };
  let rs = await methods.fetch(
    `${host}/api/comment/get-list-comments`,
    options
  );
  return rs;
};
methods.getRate = async (itemId) => {
  let options = {
    method: "POST",
    body: JSON.stringify({ itemId }),
  };
  let rs = await methods.fetch(
    `${host}/api/rate/get-rate`,
    options
  );
  return rs;
};
methods.getStoreInfo = async (storeId) => {
  let options = {
    method: "POST",
    body: JSON.stringify({ storeId }),
  };
  let rs = await methods.fetch(
    `${host}/api/store/get-store-info`,
    options
  );
  return rs;
};
methods.getLike = async (itemId) => {
  let options = {
    method: "POST",
    body: JSON.stringify({ itemId }),
  };
  let rs = await methods.fetch(
    `${host}/api/like/get-like`,
    options
  );
  return rs;
};
methods.addLike = async (itemId) => {
  let options = {
    method: "POST",
    body: JSON.stringify({ itemId }),
  };
  let rs = await methods.fetch(
    `${host}/api/like/add-like`,
    options
  );
  return rs;
};
methods.confirmOrder = async (input) => {
  let options = {
    method: "POST",
    body: JSON.stringify(input),
  };
  let rs = await methods.fetch(
    `${host}/api/v2/order/pay-order`,
    options
  );
  return rs;
};
methods.confirmOrderv1 = async (input) => {
  let options = {
    method: "POST",
    body: JSON.stringify(input),
  };
  let rs = await methods.fetch(
    `${host}/api/payment/pay-order`,
    options
  );
  return rs;
};
methods.getLinkAssetTray = async (input) => {
  let options = {
    method: "POST",
    body: JSON.stringify(input),
  };
  let rs = await methods.fetch(
    `${host}/api/code/get-web-template-for-voucher`,
    options
  );
  return rs;
};
methods.getCodeOfCustomer = async (input) => {
  let options = {
    method: "POST",
    body: JSON.stringify(input),
  };
  let rs = await methods.fetch(
    `${host}/api/code/get-codes-of-customer`,
    options
  );
  return rs;
};
methods.updateUser = async (input) => {
  let options = {
    method: "POST",
    body: JSON.stringify(input),
  };
  let rs = await methods.fetch(
    `${host}/api/customer/update-customer-info`,
    options
  );
  return rs;
};
methods.getShareCodeLink = async (input) => {
  let options = {
    method: "POST",
    body: JSON.stringify(input),
  };
  let rs = await methods.fetch(
    `${host}/api/code/get-share-voucher-url`,
    options
  );
  return rs;
};

methods.getMission = async (input) => {
  let options = {
    method: "POST",
    body: JSON.stringify(input),
  };
  let rs = await methods.fetch(
    `${host}/api/hunter/get-hunter-info`,
    options
  );
  return rs;
};

methods.getProductTypeInfo = async (productTypeId) => {
  let options = {
    useCache: true,
    method: "POST",
    url: `${host}/api/product-type/get-product-type-info`,
    body: JSON.stringify({ productTypeId }),
  };
  let rs = await callRequest(options);
  return rs;
};
methods.getItemInfo = async (itemId) => {
  let options = {
    useCache: true,
    method: "POST",
    url: `${host}/api/item/get-item-info`,
    body: JSON.stringify({ itemId }),
  };
  let rs = await callRequest(options);
  return rs;
};
methods.getShareItemUrl = async (type, typeId) => {
  let options = {
    useCache: true,
    method: "POST",
    url: `${host}/api/share/get-share-item-url`,
    body: JSON.stringify({ type, typeId }),
  };
  let rs = await callRequest(options);
  return rs;
};
methods.getPartnerHot = async (input) => {
  let options = {
    method: "POST",
    body: JSON.stringify(input),
  };
  let rs = await methods.fetch(
    `${host}/api/partner/get-list-hot-partners`,
    options
  );
  return rs;
};
methods.voucherHot = async (input) => {
  let options = {
    method: "POST",
    body: JSON.stringify(input),
  };
  let rs = await methods.fetch(
    `${host}/api/voucher/get-list-vouchers`,
    options
  );
  return rs;
};
methods.getPartnerInfo = async (input) => {
  let options = {
    method: "POST",
    body: JSON.stringify(input),
  };
  let rs = await methods.fetch(
    `${host}/api/partner/get-partner-info`,
    options
  );
  return rs;
};
methods.huntMpoint = async (input) => {
  let options = {
    method: "POST",
    body: JSON.stringify({}),
  };
  let rs = await methods.fetch(
    `${host}/api/hunter/hunt`,
    options
  );
  return rs;
};
methods.getOrderItem = async (input) => {
  let options = {
    method: "POST",
    body: JSON.stringify(input),
  };
  let rs = await methods.fetch(
    `${host}/api/ordership/get-list-order-ships`,
    options
  );
  return rs;
};

methods.rollGame = async (input) => {
  let options = {
    method: "POST",
    body: JSON.stringify(input),
  };
  let rs = await methods.fetch(
    `${host}/api/game/roll-game`,
    options
  );
  return rs;
};

methods.getGameTurn = async (input) => {
  let options = {
    method: "POST",
    body: JSON.stringify(input),
  };
  let rs = await methods.fetch(
    `${host}/api/game/game-turn`,
    options
  );
  return rs;
};
methods.redemCode = async (input) => {
  let options = {
    method: "POST",
    body: JSON.stringify(input),
  };
  let rs = await methods.fetch(
    `${host}/api/game/game-codes/redeem`,
    options
  );
  return rs;
};

methods.customerRewards = async (input) => {
  let options = {
    method: "POST",
    body: JSON.stringify(input),
  };
  let rs = await methods.fetch(
    `${host}/api/game/customer-rewards`,
    options
  );
  return rs;
};
methods.getListShopByRadius = async (input) => {
  let options = {
    method: "POST",
    body: JSON.stringify(input),
  };
  let rs = await methods.fetch(
    `${host}/api/shop/get-list-shop-by-radius`,
    options
  );
  return rs;
};
methods.orderProduct = async (input) => {
  let options = {
    method: "POST",
    body: JSON.stringify(input),
  };
  let rs = await methods.fetch(
    `${host}/api/mega/order-product`,
    options
  );
  return rs;
};
methods.reciveGameTurn = async () => {
  let options = {
    method: "POST",
    body: JSON.stringify({ gameId: 4 }),
  };
  let rs = await methods.fetch(
    `${host}/api/game/game-turns/daily-increase `,
    options
  );
  return rs;
};
methods.getProductLiked = async (input) => {
  let options = {
    method: "POST",
    body: JSON.stringify(input),
  };
  let rs = await methods.fetch(
    `${host}/api/product-type/get-liked-product-type`,
    options
  );
  return rs;
};
methods.caculateOrder = async (input) => {
  let options = {
    method: "POST",
    body: JSON.stringify(input),
  };
  let rs = await methods.fetch(
    `${host}/api/v2/order/calculate-price`,
    options
  );
  return rs;
};
methods.cancelOrder = async (input) => {
  let options = {
    method: "POST",
    body: JSON.stringify(input),
  };
  let rs = await methods.fetch(
    `${host}/api/v2/order/cancel-order`,
    options
  );
  return rs;
};
methods.sendOtp = async (input) => {
  let options = {
    method: "POST",
    body: JSON.stringify(input),
  };
  let rs = await methods.fetch(
    `${host}/api/otp/send-otp`,
    options
  );
  return rs;
};
methods.verifyOtp = async (input) => {
  let options = {
    method: "POST",
    body: JSON.stringify(input),
  };
  let rs = await methods.fetch(
    `${host}/api/otp/verify-otp`,
    options
  );
  return rs;
};
methods.loginByPhone = async (input) => {
  let options = {
    method: "POST",
    body: JSON.stringify(input),
  };
  let rs = await methods.fetch(
    `${host}/api/customer/login-by-phone`,
    options
  );
  return rs;
};
methods.renewToken = async (input) => {
  let options = {
    method: "POST",
    body: JSON.stringify(input),
  };
  let rs = await methods.fetch(
    `${host}/api/customer/renew-token`,
    options
  );
  return rs;
};
methods.registerDevice = async (input) => {
  let options = {
    method: "POST",
    body: JSON.stringify(input),
  };
  let rs = await methods.fetch(
    `${host}/api/device/register-device`,
    options
  );
  return rs;
};
methods.getListNotification = async (input) => {
  let options = {
    method: "POST",
    body: JSON.stringify(input),
  };
  let rs = await methods.fetch(
    `${host}/api/notification/get-all-notification`,
    options
  );
  return rs;
};
methods.getNotiCount = async () => {
  let options = {
    method: "POST",
  };
  let rs = await methods.fetch(
    `${host}/api/notification/get-noti-count`,
    options
  );
  return rs;
};
methods.updateIsRead = async (input) => {
  let options = {
    method: "POST",
    body: JSON.stringify(input),
  };
  let rs = await methods.fetch(
    `${host}/api/notification/update-is-read`,
    options
  );
  return rs;
};
methods.createTransactionCode = async () => {
  let options = {
    method: "POST",
    body: JSON.stringify({}),
  };
  let rs = await methods.fetch(
    `${host}/api/exchange/create-transaction-code`,
    options
  );
  return rs;
};
methods.useBillingCode = async (code) => {
  let options = {
    method: "POST",
    body: JSON.stringify({ code }),
  };
  let rs = await methods.fetch(
    `${host}/api/exchange/using-billing-code`,
    options
  );
  return rs;
};
methods.chekUserExit = async (phone) => {
  let options = {
    method: "POST",
    body: JSON.stringify({ phone }),
  };
  let rs = await methods.fetch(
    `${host}/api/customer/check-user-exist`,
    options
  );
  return rs;
};
methods.resetPassword = async ({ password }) => {
  let options = {
    method: "POST",
    body: JSON.stringify({ password }),
  };
  let rs = await methods.fetch(
    `${host}/api/customer/set-password`,
    options
  );
  return rs;
};
methods.loginByPassword = async ({ password, phone }) => {
  let options = {
    method: "POST",
    body: JSON.stringify({ password, phone }),
  };
  let rs = await methods.fetch(
    `${host}/api/customer/login-by-password `,
    options
  );
  return rs;
};
methods.getCustomerAddress = async () => {
  let options = {
    method: "POST",
    body: JSON.stringify({}),
  };
  let rs = await methods.fetch(
    `${host}/api/shipingaddress/get-customer-shiping-address`,
    options
  );
  return rs;
};
methods.createAddress = async (data) => {
  let options = {
    method: "POST",
    body: JSON.stringify(data),
  };
  let rs = await methods.fetch(
    `${host}/api/shipingaddress/create-customer-shiping-address`,
    options
  );
  return rs;
};
methods.deleteAddress = async (shipingAddressId) => {
  let options = {
    method: "POST",
    body: JSON.stringify({ shipingAddressId }),
  };
  let rs = await methods.fetch(
    `${host}/api/shipingaddress/delete-customer-shiping-address`,
    options
  );
  return rs;
};
methods.getTxtFrame = async () => {
  let options = {
    method: "GET",
  };
  let rs = await methods.fetch(
    `${host}/api/game/get-run-message-win-reward`,
    options
  );
  return rs;
};
methods.getProductPopup = async (data) => {
  let options = {
    method: "POST",
    body: JSON.stringify(data),
  };
  let rs = await methods.fetch(
    `${host}/api/popupitem/get-data-items`,
    options
  );
  return rs;
};

methods.getListCampaigns = async () => {
  let options = {
    method: "POST",
  };
  let rs = await methods.fetch(
    `${host}/api/campaign/get-list-campaigns`,
    options
  );
  return rs;
};

methods.createCaptcha = async () => {
  let options = {
    method: "POST",
  };
  let rs = await methods.fetch(
    `${host}/api/user/create-captcha`,
    options
  );
  return rs;
};

methods.addCardLpoint = async (data) => {
  let options = {
    method: "POST",
    body: JSON.stringify(data),
  };
  let rs = await methods.fetch(
    `${host}/api/card/add-l-point-card`,
    options
  );
  return rs;
};

methods.getVoucherLike = async (data) => {
  let options = {
    method: "POST",
    body: JSON.stringify(data),
  };
  let rs = await methods.fetch(
    `${host}/api/voucher/get-liked-vouchers `,
    options
  );
  return rs;
};
methods.getProductTypeInfo = async (productTypeId) => {
  let options = {
    method: "POST",
    body: JSON.stringify({ productTypeId }),
  };
  let rs = await methods.fetch(
    `${host}/api/product-type/get-product-type-info`,
    options
  );
  return rs;
};
methods.regiterCustomer = async (option) => {
  let options = {
    method: "POST",
    body: JSON.stringify(option),
  };
  let rs = await methods.fetch(
    `${host}/api/customer/register`,
    options
  );
  return rs;
};
methods.createOrder = async (option) => {
  let options = {
    method: "POST",
    body: JSON.stringify(option),
  };
  let rs = await methods.fetch(
    `${host}/api/order/create-order`,
    options
  );
  return rs;
};
methods.payOrder = async (orderId) => {
  let options = {
    method: "POST",
    body: JSON.stringify({orderId}),
  };
  let rs = await methods.fetch(
    `${host}/api/order/pay-order`,
    options
  );
  return rs;
};
methods.getListOrder = async (status) => {
  let options = {
    method: "POST",
    body: JSON.stringify({
      skip : 0,
      limit : 10,
      status
    }),
  };
  let rs = await methods.fetch(
    `${host}/api/order/get-order-info`,
    options
  );
  return rs;
};
methods.getListProductTypes = async (input) => {
  let options = {
    method: "POST",
    body: JSON.stringify(input),
  };
  let rs = await methods.fetch(`${host}/api/product-type/get-list-product-type`,options);
  return rs;
};
methods.getCategories = async () => {
  let options = {
    method: "GET",
  };
  let rs = await methods.fetch(`${host}/api/category/get`,options);
  console.log(rs)
  return rs;
};

export default methods;
